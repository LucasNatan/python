def multi(n1, n2):
    if n2 == 1:
        return n1
    else:
        return n1 + multi(n1, n2 - 1)


def ponte(n1, n2):
    if n2 == 0:
        return 1
    else:
        return n1 * ponte(n1, n2 - 1)


def soma(n1):
    if n1 == 1:
        return n1
    else:
        return n1 + soma(n1 - 1)
        
def firstAlga(n1):
    if n1 <= 9:
        return n1
    else:
        return firstAlga(n1 // 10)


def removeN(lista, n):
    if n not in lista:
        return lista
    else:
        lista = lista.pop[n]
        return removeN(lista, n)


def removeN(lista, n):
    if n not in lista:
        return lista
    else:
        lista.remove(n)
        return removeN(lista, n)


def numPerfect(n, cont=1, summ=0):
    if cont == n:
        return summ == n
    if n % cont == 0:
        summ += cont
    return numPerfect(n, cont + 1, summ)