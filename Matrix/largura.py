"""
Busca em Largura
Visita-se um vertice, selecionado de forma aleatória. Em seguida, o vertice é marcado como visitado e inserido em uma fila q;
    Enquanto a fila q não estiver vazia:
    Um vertice n é retirado da fila q;

    Para cada vertice m (não visitado) que for adjacente a n;
        O vertice m é visitado;
        O vertice m é colocado na fila q;

"""
visitados = []
fila = []

grafo = [
    [0, 1, 2],
    [1, 0, 3, 4],
    [2, 0, 5, 6],
    [3, 1],
    [4, 1],
    [5, 2],
    [6, 2]]


def adjacentes_vertice(v, lista):
    adj = []
    for i in range(len(lista)):
        if lista[i][0] == v:
            adj = lista[i][1:]
    return adj



def run(vertice):
    visitados.append(vertice)
    fila.append(vertice)

    while len(fila):
        q = fila.pop(vertice)

        for m in adjacentes_vertice(q, grafo):
            if m not in visitados:
                visitados.append(m)
                fila.append(m)


inicio = int(input('Qual Vertice deseja começar? '))
run(inicio)
print(visitados)

