print("Encontrar a solução de sistemas lineares de ordem 2 e 3, através do método de Cramer")

z = int(input('Informe o tipo de Matriz  2 ou 3 Incógnitas!'))
if z == 3:
   print("\n a11 a12 a13 = b1 \n a21 a22 a23 = b2 \n a31 a32 a33 = b3 \n")
   print("Informe os valores para:")
   a11 = int(input("a11 = "))
   a12 = int(input("a12 = "))
   a13 = int(input("a12 = "))
   b1 = int(input("b1 = "))

   a21 = int(input("\na21 = "))
   a22 = int(input("a22 = "))
   a23 = int(input("a32 = "))
   b2 = int(input("b2 = "))

   a31 = int(input("\na31 = "))
   a32 = int(input("a32 = "))
   a33 = int(input("a33 = "))
   b3 = int(input("b3 = "))

   det = ((a11 * a22 * a33) + (a12 * a23 * a31) + (a13 * a21 * a32)) - \
         ((a13 * a22 * a31) + (a11 * a23 * a32) + (a12 * a21 * a33))  # Determinante da Matriz

   detX = ((b1 * a22 * a33) + (a12 * a23 * b3) + (a13 * b2 * a32)) - \
          ((a13 * a22 * b3) + (b1 * a23 * a32) + (a12 * b2 * a33))  # Determinante X

   detY = ((a11 * b2 * a33) + (b1 * a23 * a31) + (a13 * a21 * b3)) - \
          ((a13 * b2 * a31) + (a11 * a23 * b3) + (b1 * a21 * a33))  # Determinante Y

   detZ = ((a11 * a22 * b3) + (a12 * b2 * a31) + (b1 * a21 * a32)) - \
          ((b1 * a22 * a31) + (a11 * b2 * a32) + (a12 * a21 * b3))  # Determinante Z
   z = detZ / det
   x = detX / det
   y = detY / det
   print('\nx = {} \ny = {} \n z = {}'.format(x, y, z))

   if det != 0:
       print('Sistema Possível e Determinado (SPD)')
   elif (det == 0) and (detX == 0) and (detY == 0):
       print('Infinitas soluções SPI')
   else:
       print('Não Existe soluções SI')

else:
   print("\n a11 a12 = b1 \n a21 a22 = b2 \n")
   print("Informe os valores para: ")
   a11 = int(input("a11 = "))
   a12 = int(input("a12 = "))
   b1 = int(input("b1 = "))

   a21 = int(input("\na21 = "))
   a22 = int(input("a22 = "))
   b2 = int(input("b2 = "))

   det = (a11 * a12) - (a12 * a22)  # Determinante da Matriz
   detX = (b1 * a22) - (b2 * a12)  # Determinante X
   detY = (a11 * b2) - (a21 * b1)  # Determinante Y
   x = detX / det
   y = detY / det
   print('\nx = {} \ny = {}'.format(x, y))

   if det != 0:
       print('Sistema Possível e Determinado (SPD)')
   elif (det == 0) and (detX == 0) and (detY == 0):
       print('Infinitas soluções SPI')
   else:
       print('Não Existe soluções SI')
